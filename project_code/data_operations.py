import os
import pandas as pd
import numpy as np
import json

# local path
base_path = "./../pan24-multi-author-analysis/"
easy_train_path = os.path.join(base_path, "train/easy")
easy_validation_path = os.path.join(base_path, "validation/easy")
medium_train_path = os.path.join(base_path, "train/medium")
medium_validation_path = os.path.join(base_path, "validation/medium")
hard_train_path = os.path.join(base_path, "train/hard")
hard_validation_path = os.path.join(base_path, "validation/hard")

def read_input_data(path_to_data):
    """
    Reads the input data from the path passed as a parameter. The function can be used for train and validation,
    where there is a problem and a truth file.
    """
    # read input data in as dictionary
    dict_data = dict()
    for filename in os.listdir(path_to_data):
        if filename.endswith(".txt"):
            problem_id = filename.split("-")[1].strip('.txt')  # Extract problem ID
            try: 
                problem_id = int(problem_id)
                with open(os.path.join(path_to_data, filename), "r", newline="", encoding="utf8") as file:
                    text = file.read().split('\n')
                with open(os.path.join(path_to_data, f"truth-{filename[:-4]}.json"), "r") as file:
                    truth_data = json.load(file)
                dict_data[problem_id] = {"text": text, "authors": truth_data["authors"], "changes": truth_data["changes"]}
            except ValueError:
                pass
    
    # return dictionary as dataframe
    return pd.DataFrame.from_dict(dict_data).transpose()


def generate_paragraph_pairs(df):
    """
    Generates a dataframe where paragraphs are ordered in pairs and labeled to suggest the change.
    """
    paragraph_pairs = pd.DataFrame()
    paragraph_1 = []
    paragraph_2 = []
    change = []
    # loop through all the input files
    for input_index in df.index:
        paragraphs = df.loc[input_index]['text']
        for par_index in range(len(paragraphs)):
            paragraph_1.append(paragraphs[par_index - 1])
            paragraph_2.append(paragraphs[par_index])
            change.append(df.loc[input_index]['changes'][par_index - 1])
    
    paragraph_pairs['paragraph_1'] = paragraph_1
    paragraph_pairs['paragraph_2'] = paragraph_2
    paragraph_pairs['label'] = change

    return paragraph_pairs
    

def print_change_no_change_nr(paragraph_pairs):
    """
    Returns the number of changes and no changes on paragraph pair level, present in the dataset.
    """
    print("(#Change, #No Change) =", (paragraph_pairs['label'].sum(), len(paragraph_pairs['label']) - paragraph_pairs['label'].sum()))
    print("Total nr of paragraph pairs: ", len(paragraph_pairs))


def get_maximum_seq_len(df, tokenizer):
    """
    Calculates maximmum sequence length (excluding [CLS], [SEP]).
    """
    # number of tokens in each paragraph
    seq_lens = []
    nr_exceeding_pars = 0
    for paragraphs in df['text']:
        # calculate maximum sequence length of an input file
        curr_lens = [len(tokenizer.encode(par)) for par in paragraphs]
        for curr_len in curr_lens:
            if curr_len > 512:
                nr_exceeding_pars += 1
        seq_lens.extend(curr_lens)
    print("Maximum sequence length: ", np.max(seq_lens))
    print("Average seq length: ", np.mean(seq_lens))
    print("Number of paragraphs exceeding the maximum length: ", nr_exceeding_pars)
