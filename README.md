# Automatic Writing Style Analysis


The repository contains the code for the Natural Language Processing 2 exam project. You can find more information on the exam task here: https://pan.webis.de/clef24/pan24-web/style-change-detection.html.

## Data
You can find a zip file in the repository with the input data. Once extracted, the dataset should be placed in the same folder with the implementation files.

## Environment
+ All the python notebooks contain a section entitled *General settings for the Google Colab environment*, which sets the environment for running the notebook in Google Colab.
